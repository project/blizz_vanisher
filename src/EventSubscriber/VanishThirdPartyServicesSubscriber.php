<?php

namespace Drupal\blizz_vanisher\EventSubscriber;

use Drupal\blizz_vanisher\Service\ThirdPartyServicesVanisher;
use Drupal\Core\Render\HtmlResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class VanishThirdPartyServicesSubscriber.
 *
 * @package Drupal\blizz_vanisher\EventSubscriber
 */
class VanishThirdPartyServicesSubscriber implements EventSubscriberInterface {

  /**
   * The vanisher to vanish the third party services from the content.
   *
   * @var \Drupal\blizz_vanisher\Service\ThirdPartyServicesVanisher
   */
  protected $vanisher;

  /**
   * VanishThirdPartyServicesSubscriber constructor.
   *
   * @param \Drupal\blizz_vanisher\Service\ThirdPartyServicesVanisher $vanisher
   *   The third party services vanisher.
   */
  public function __construct(ThirdPartyServicesVanisher $vanisher) {
    $this->vanisher = $vanisher;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[KernelEvents::RESPONSE][] = ['onResponse'];

    return $events;
  }

  /**
   * Callback method that will be executed before the response will be send.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   The filter response event.
   */
  public function onResponse(FilterResponseEvent $event) {
    if (!$event->getResponse() instanceof HtmlResponse) {
      return;
    }
    $content = $event->getResponse()->getContent();
    $content = $this->vanisher->vanish($content);
    $event->getResponse()->setContent($content);
  }

}
