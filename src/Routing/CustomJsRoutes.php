<?php

namespace Drupal\blizz_vanisher\Routing;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 *
 */
class CustomJsRoutes {

  /**
   * {@inheritdoc}
   */
  public function routes() {

    $route_collection = new RouteCollection();

    $language_manager = \Drupal::service('language_manager');
    $languages = $language_manager->getLanguages();
    foreach ($languages as $language) {

      // Todo find a automatic find solution.
      $pathes[$language->getId()] = "/libraries/tarteaucitron/lang/tarteaucitron." . $language->getId() . ".js";
    }

    foreach ($pathes as $langcode => $path) {
      $route = new Route(
        $path,
        [
          '_controller' => 'blizz_vanisher.language_js_controller:getRouteAction',
          'langcode' => $langcode,
        ],
        [
          '_permission' => 'access content',
        ]
      );

      $route_collection->add('tarteaucitron.lang.' . $langcode, $route);

    }

    $route = new Route(
      "/libraries/tarteaucitron/tarteaucitron.services.js",
      [
        '_controller' => 'blizz_vanisher.language_js_controller:getServiceRoute',
      ],
      [
        '_permission' => 'access content',
      ]
    );
    $route_collection->add('tarteaucitron.services', $route);

    return $route_collection;
  }

}
