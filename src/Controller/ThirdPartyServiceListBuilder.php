<?php

namespace Drupal\blizz_vanisher\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of third Party Service entities.
 *
 * @package Drupal\blizz_vanisher\Controller
 */
class ThirdPartyServiceListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['name'] = $this->t('Name');
    $header['vanisher'] = $this->t('Vanisher');
    $header['enabled'] = $this->t('Activated');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\blizz_vanisher\Entity\ThirdPartyServiceEntityInterface $third_party_service */
    $third_party_service = $entity;

    $row = [];
    $row['name'] = $third_party_service->getName();
    $row['vanisher'] = $third_party_service->getVanisher();
    $row['enabled'] = $third_party_service->isEnabled() ? $this->t('Enabled') : $this->t('Disabled');

    return $row + parent::buildRow($third_party_service);
  }

}
