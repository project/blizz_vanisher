<?php

namespace Drupal\blizz_vanisher\Controller;

use Drupal\blizz_vanisher\Form\LanguageSettingsForm;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides a listing of third Party Service entities.
 *
 * @package Drupal\blizz_vanisher\Controller
 */
class LanguageJsController extends ControllerBase {

  /**
   *
   */
  public function getRouteAction($langcode) {
    // Todo.
    $language_manager = \Drupal::service('language_manager');
    $language = $language_manager->getLanguage($langcode);
    $original_language = $language_manager->getConfigOverrideLanguage();
    $language_manager->setConfigOverrideLanguage($language);
    $config = \Drupal::config(LanguageSettingsForm::SETTINGS_NAME);
    $values = $config->get();
    $language_manager->setConfigOverrideLanguage($original_language);

    unset($values['langcode']);
    $json_string = json_encode($values);

    $response = new Response('tarteaucitron.lang =' . $json_string, 200, ['Content-Type' => 'application/javascript']);
    return $response;
  }

  /**
   *
   */
  public function getServiceRoute() {
    $service_data = [];

    $services = \Drupal::entityTypeManager()
      ->getStorage('third_party_service')
      ->loadByProperties(['enabled' => TRUE]);

    $thirdparty = \Drupal::service('blizz_vanisher.service.third_party_services_vanisher');

    foreach ($services as $service) {

      if (!$thirdparty->hasVanisher($service->getVanisher())) {
        // TODO: Log warning.
        continue;
      }

      $vanisher_name = $service->getVanisher();
      $vanisher_id = $vanisher_name . '.' . $service->getId();

      // The vanished script.
      $vanisher = $thirdparty->getVanisher($vanisher_name);
      if ($vanisher) {
        $service_element = [];
        $service_element["key"] = $vanisher->getVanisherName();
        $service_element["type"] = $service->getGroupType();
        $service_element["name"] = $service->getName();
        $service_element["description"] = $service->getDescription();
        $service_element["read_more"] = $service->getReadMore();
        $service_element["uri"] = $service->getLinkToOfficialWebsite();
        $service_element["needConsent"] = (boolean) $service->needConsent();
        $service_element["cookies"] = $vanisher->getCookies();
        if ($vanisher->getFallbackJavascript() !== '') {
          $service_element["fallback"] = "<$".$vanisher->getVanisherName()."==>";
        }
        $service_element["js"] = "<!".$vanisher->getVanisherName()."==>";
        $service_data[$vanisher->getVanisherName()] = $service_element;
        $service_vanisher[$vanisher->getVanisherName()] = $vanisher;
      }
    }

    $json_string = json_encode($service_data);

    foreach ($service_data as $vanisher_name => $service) {
      $json_string = str_replace('"<!' . $vanisher_name . '==>"',  $service_vanisher[$vanisher_name]->getJavascript(), $json_string);
      $json_string = str_replace('"<$' . $vanisher_name . '==>"', $service_vanisher[$vanisher_name]->getFallbackJavascript(), $json_string);
    }
    $response = new Response('    tarteaucitron.services =' . $json_string, 200, ['Content-Type' => 'application/javascript']);
    return $response;

  }

}
