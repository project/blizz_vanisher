<?php

namespace Drupal\blizz_vanisher\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * @ConfigEntityType(
 *   id = "third_party_service",
 *   label = @Translation("Third Party Service Entity"),
 *   handlers = {
 *     "list_builder" = "Drupal\blizz_vanisher\Controller\ThirdPartyServiceListBuilder",
 *     "form" = {
 *       "add" = "Drupal\blizz_vanisher\Form\ThirdPartyServiceForm",
 *       "edit" = "Drupal\blizz_vanisher\Form\ThirdPartyServiceForm",
 *       "delete" = "Drupal\blizz_vanisher\Form\ThirdPartyServiceDeleteForm"
 *     },
 *   },
 *   config_prefix = "third_party_service",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "name" = "name",
 *     "info" = "info",
 *     "description" = "description",
 *     "enabled" = "enabled",
 *     "vanisher" = "vanisher",
 *     "readMore" = "readMore",
 *     "needConsent" = "needConsent",
 *     "uri" = "uri",
 *     "type" = "type"
 *   },
 *   list_cache_tags = {
 *     "blizz_vanisher",
 *     "third_party_service"
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/system/blizz-vanisher/third-party-service/{third_party_service}",
 *     "delete-form" = "/admin/config/system/blizz-vanisher/third-party-service/{third_party_service}/delete"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "name",
 *     "info",
 *     "description",
 *     "enabled",
 *     "vanisher",
 *     "readMore",
 *     "needConsent",
 *     "uri",
 *     "type"
 *   }
 * )
 */
class ThirdPartyServiceEntity extends ConfigEntityBase implements ThirdPartyServiceEntityInterface {

  /**
   * The third party service id.
   *
   * @var string
   */
  protected $id = '';

  /**
   * The label.
   *
   * @var string
   */
  protected $label = '';

  /**
   * The name of the third party service.
   *
   * @var string
   */
  protected $name = '';

  /**
   * The info content of the third party service.
   *
   * @var string
   */
  protected $info = '';

  /**
   * The read more of the third party service.
   *
   * @var string
   */
  protected $readMore = '';


  /**
   * The uri of the third party service.
   *
   * @var string
   */
  protected $uri = '';


  /**
   * The flag indicating if service control is activated.
   *
   * @var bool
   */
  protected $enabled = FALSE;

  /**
   * The name of the vanisher.
   *
   * @var string
   */
  protected $vanisher = '';

  /**
   * Check for requirement need consent.
   *
   * @var string
   */
  protected $needConsent = TRUE;


  /**
   * Check for requirement need consent.
   *
   * @var string
   */
  protected $type = 'api';

  /**
   * Check for requirement need consent.
   *
   * @var string
   */
  protected $description = '';

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return $this->info;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return $this->enabled;
  }

  /**
   * {@inheritdoc}
   */
  public function getVanisher() {
    return $this->vanisher;
  }

  /**
   * {@inheritdoc}
   */
  public function getReadMore() {
    return $this->readMore;
  }

  /**
   * {@inheritdoc}
   */
  public function getLinkToOfficialWebsite() {
    return $this->uri;
  }

  /**
   * {@inheritdoc}
   */
  public function needConsent() {
    return $this->needConsent;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupType() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

}
