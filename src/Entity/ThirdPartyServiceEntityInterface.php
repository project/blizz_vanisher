<?php

namespace Drupal\blizz_vanisher\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface ThirdPartyServiceEntityInterface.
 *
 * @package Drupal\blizz_vanisher\Entity
 */
interface ThirdPartyServiceEntityInterface extends ConfigEntityInterface {

  /**
   * Returns the entity id.
   *
   * @return string
   *   The entity id.
   */
  public function getId(): string;

  /**
   * Returns the label.
   *
   * @return string
   *   The label.
   */
  public function getLabel(): string;

  /**
   * Returns the name.
   *
   * @return string
   *   The name.
   */
  public function getName(): string;

  /**
   * Returns the info content.
   *
   * @return string
   *   The info content.
   */
  public function getInfo();

  /**
   * Returns whether the service will be controlled or not.
   *
   * @return bool
   *   TRUE when control is activated, otherwise FALSE.
   */
  public function isEnabled();

  /**
   * Returns the name of the vanisher to use.
   *
   * @return string
   *   The name of the vanisher.
   */
  public function getVanisher();

  /**
   * Returns the name of the vanisher to use.
   *
   * @return string
   *   The name of the vanisher.
   */
  public function getReadMore();

  /**
   * Returns the name of the vanisher to use.
   *
   * @return string
   *   The name of the vanisher.
   */
  public function getLinkToOfficialWebsite();

  /**
   * Returns the name of the vanisher to use.
   *
   * @return string
   *   The name of the vanisher.
   */
  public function needConsent();

  /**
   * Returns the name of the vanisher to use.
   *
   * @return string
   *   The name of the vanisher.
   */
  public function getGroupType();

  /**
   * Returns the name of the vanisher to use.
   *
   * @return string
   *   The name of the vanisher.
   */
  public function getDescription();

}
