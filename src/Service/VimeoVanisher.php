<?php

namespace Drupal\blizz_vanisher\Service;

/**
 * Class VimeoVanisher.
 *
 * @package Drupal\blizz_vanisher\Service
 */
class VimeoVanisher extends EmbeddedVideoVanisher {

  /**
   * The regular expression to find the video id inside of a vimeo url.
   *
   * @see https://gist.github.com/anjan011/1fcecdc236594e6d700f
   */
  const VIMEO_VIDEO_ID_REGEX = '~^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$~i';

  /**
   * {@inheritdoc}
   */
  protected function getIframeSearchRegexPattern() {
    return '~(<iframe.*?src=([\'"])(.*?player\.vimeo\.com\/video\/.*?)\2.*?>.*?<\/iframe>)~i';
  }

  /**
   * {@inheritdoc}
   */
  protected function getReplacementScript() {
    return '(tarteaucitron.job = tarteaucitron.job || []).push(\'vimeo\');';
  }

  /**
   * {@inheritdoc}
   */
  protected function getReplacementMarkupTemplate() {
    return '<div class="vimeo_player" videoID="@video_id" width="@width" height="@height"></div>';
  }

  /**
   * {@inheritdoc}
   */
  protected function getVideoData($markup) {
    $data = parent::getVideoData($markup);
    $data['video_id'] = $this->extractVideoId($data['src']);

    return $data;
  }

  /**
   * Extracts the video id.
   *
   * @param string $url
   *   The video url containing the video id.
   *
   * @return string|null
   *   The video id or NULL.
   */
  protected function extractVideoId($url) {
    $matches = [];
    $ret = preg_match(self::VIMEO_VIDEO_ID_REGEX, $url, $matches);
    if ($ret != FALSE && $ret == 1) {
      return $matches[3];
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getVanisherName() {
    return 'vimeo';
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return 'Vimeo Vanisher';
  }

  /**
   *
   */
  public function getCookies() {
    return ['__utmt_player', '__utma', '__utmb', '__utmc', '__utmv', 'vuid', '__utmz', 'player'];
  }

  /**
   *
   */
  public function getJavascript() {
    return <<< EOT
 function () {
  "use strict";
  tarteaucitron.fallback(['vimeo_player'], function (x) {
    var video_id = x.getAttribute("videoID"),
                video_width = x.getAttribute("width"),
                frame_width = 'width=',
                video_height = x.getAttribute("height"),
                frame_height = 'height=',
                video_frame;
            
            if (video_id === undefined) {
              return "";
            }
            if (video_width !== undefined) {
              frame_width += '"' + video_width + '" ';
            } else {
              frame_width += '"" ';
            }
            if (video_height !== undefined) {
              frame_height +=  '"' + video_height + '" ';
            } else {
              frame_height += '"" ';
            }
            video_frame = '<iframe src="//player.vimeo.com/video/' + video_id + '" ' + frame_width + frame_height + ' frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
            return video_frame;
        });
}
EOT;
  }

  /**
   *
   */
  public function getFallbackJavascript() {
    return <<<EOT
function () {
  "use strict";
  var id = 'vimeo';
  tarteaucitron.fallback(['vimeo_player'], function (elem) {
    elem.style.width = elem.getAttribute('width') + 'px';
    elem.style.height = elem.getAttribute('height') + 'px';
    return tarteaucitron.engage(id);
  });
}
EOT;

  }

}
