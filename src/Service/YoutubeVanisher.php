<?php

namespace Drupal\blizz_vanisher\Service;

/**
 * Class YoutubeVanisher.
 *
 * @package Drupal\blizz_vanisher\Service
 */
class YoutubeVanisher extends EmbeddedVideoVanisher {

  /**
   * The regular expression to find the video id inside of a youtube url.
   *
   * @see https://stackoverflow.com/a/9102270/2779907
   */
  const YOUTUBE_VIDEO_ID_REGEX = '~^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*~i';

  /**
   * {@inheritdoc}
   */
  protected function getReplacementMarkupTemplate() {
    return '<div class="youtube_player" videoID="@video_id" width="@width" height="@height"></div>@info_text';
  }

  /**
   * {@inheritdoc}
   */
  protected function getIframeSearchRegexPattern() {
    return '~(<iframe[^>]*?src=[^>]*?youtu.*?>.*?</iframe>)~is';
  }

  /**
   * {@inheritdoc}
   */
  protected function getReplacementScript() {
    return '(tarteaucitron.job = tarteaucitron.job || []).push(\'youtube\');';
  }

  /**
   * {@inheritdoc}
   */
  protected function getVideoData($markup) {
    $data = parent::getVideoData($markup);
    $data['video_id'] = $this->extractVideoId($data['src']);

    return $data;
  }

  /**
   * Extracts the video id.
   *
   * @param string $url
   *   The video url containing the video id.
   *
   * @return string|null
   *   The video id or NULL.
   */
  protected function extractVideoId($url) {
    $matches = [];
    $ret = preg_match(self::YOUTUBE_VIDEO_ID_REGEX, $url, $matches);
    if ($ret != FALSE && $ret == 1) {
      return $matches[2];
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getVanisherName() {
    return 'youtube';
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return 'Youtube Vanisher';
  }

  /**
   *
   */
  public function getCookies() {
    return ['VISITOR_INFO1_LIVE', 'YSC', 'PREF', 'GEUP'];
  }

  /**
   *
   */
  public function getJavascript() {
    return <<< JS
function () {
      "use strict";
      tarteaucitron.fallback(['youtube_player'], function (x) {
        var video_id = x.getAttribute("videoID"),
                video_width = x.getAttribute("width"),
                frame_width = 'width=',
                video_height = x.getAttribute("height"),
                frame_height = 'height=',
                video_frame,
                params = 'theme=' + x.getAttribute("theme") + '&rel=' + x.getAttribute("rel") + '&controls=' + x.getAttribute("controls") + '&showinfo=' + x.getAttribute("showinfo") + '&autoplay=' + x.getAttribute("autoplay");

            if (video_id === undefined) {
              return "";
            }
            if (video_width !== undefined) {
              frame_width += '"' + video_width + '" ';
            } else {
              frame_width += '"" ';
            }
            if (video_height !== undefined) {
              frame_height +=  '"' + video_height + '" ';
            } else {
              frame_height += '"" ';
            }
            video_frame = '<iframe type="text/html" ' + frame_width + frame_height + ' src="//www.youtube-nocookie.com/embed/' + video_id + '?' + params + '" frameborder="0" allowfullscreen></iframe>';
            return video_frame;
        });
    }
JS;
  }

  /**
   *
   */
  public function getFallbackJavascript() {
    return "function () {
      var id = 'youtube';
      tarteaucitron.fallback(['youtube_player'], function (elem) {
        elem.style.width = elem.getAttribute('width') + 'px';
        elem.style.height = elem.getAttribute('height') + 'px';
        return tarteaucitron.engage(id);
      });
    }";
  }

}
