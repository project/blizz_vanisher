<?php

namespace Drupal\blizz_vanisher\Service;

/**
 * Class DailymotionVanisher.
 *
 * @package Drupal\blizz_vanisher\Service
 */
class DailymotionVanisher extends EmbeddedVideoVanisher {

  /**
   * The regular expression to find the video id inside of a Dailymotion url.
   *
   * @see https://stackoverflow.com/a/9102270/2779907
   */
  const DAILYMOTION_VIDEO_ID_REGEX = '~^.*(dailymotion\.com\/|v\/|u\/\w\/|embed\/video\/|watch\?v=|\&v=)([^#\&\?]*).*~i';

  /**
   * {@inheritdoc}
   */
  protected function getReplacementMarkupTemplate() {
    return '<div class="dailymotion_player" videoID="@video_id" width="@width" height="@height"></div>@info_text';
  }

  /**
   * {@inheritdoc}
   */
  protected function getIframeSearchRegexPattern() {
    return '~(<iframe[^>]*?src=[^>]*?dailymotion.*?>.*?</iframe>)~is';
  }

  /**
   * {@inheritdoc}
   */
  protected function getReplacementScript() {
    return '(tarteaucitron.job = tarteaucitron.job || []).push(\'dailymotion\');';
  }

  /**
   * {@inheritdoc}
   */
  protected function getVideoData($markup) {
    $data = parent::getVideoData($markup);
    $data['video_id'] = $this->extractVideoId($data['src']);

    return $data;
  }

  /**
   * Extracts the video id.
   *
   * @param string $url
   *   The video url containing the video id.
   *
   * @return string|null
   *   The video id or NULL.
   */
  protected function extractVideoId($url) {
    $matches = array();
    $ret = preg_match(self::DAILYMOTION_VIDEO_ID_REGEX, $url, $matches);
    if ($ret != FALSE && $ret == 1) {
      return $matches[2];
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getVanisherName() {
    return 'dailymotion_vanisher';
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return 'Dailymotion Vanisher';
  }

  /**
   * @return array|string[]
   */
  public function getCookies()
  {
    return ['ts', 'dmvk', 'hist', 'v1st', 's_vi'];
  }

  /**
   * Returns the name of this vanisher.
   *
   * @return string
   *   The name of this vanisher.
   */
  public function getJavascript()
  {
    return <<< EOT
function () {
        "use strict";
        tarteaucitron.fallback(['dailymotion_player'], function (x) {
            var video_id = x.getAttribute("videoID"),
                video_width = x.getAttribute("width"),
                frame_width = 'width=',
                video_height = x.getAttribute("height"),
                frame_height = 'height=',
                video_frame,
                params = 'info=' + x.getAttribute("showinfo") + '&autoPlay=' + x.getAttribute("autoplay");

            if (video_id === undefined) {
                return "";
            }
            if (video_width !== undefined) {
                frame_width += '"' + video_width + '" ';
            } else {
                frame_width += '"" ';
            }
            if (video_height !== undefined) {
                frame_height +=  '"' + video_height + '" ';
            } else {
                frame_height += '"" ';
            }
            video_frame = '<iframe src="//www.dailymotion.com/embed/video/' + video_id + '?' + params + '" ' + frame_width + frame_height + ' frameborder="0" allowfullscreen></iframe>';
            return video_frame;
        });
    }
EOT;
  }

  /**
   * Returns the name of this vanisher.
   *
   * @return string
   *   The name of this vanisher.
   */
  public function getFallbackJavascript()
  {
    return <<<EOT
function () {
        "use strict";
        var id = 'facebook';
        tarteaucitron.fallback(['fb-post', 'fb-follow', 'fb-activity', 'fb-send', 'fb-share-button', 'fb-like'], tarteaucitron.engage(id));
    }
EOT;
  }
}
