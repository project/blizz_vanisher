<?php

namespace Drupal\blizz_vanisher\Service;

/**
 * Class FacebookLikesVanisher.
 *
 * @package Drupal\blizz_vanisher\Service
 */
class FacebookLikesVanisher extends ThirdPartyServicesVanisher implements ThirdPartyServicesVanisherInterface {

  /**
   * {@inheritdoc}
   */
  public function vanish(&$content) {
    $script = $this->getScript('connect.facebook.net', $this->getAllScripts($content));

    // Remove the script from the content.
    $content = $this->removeScript($script, $content);

    return $this->getReplacementScript();
  }

  /**
   * Returns the replacement script.
   *
   * @return string
   *   The replacement script.
   */
  public function getReplacementScript() {
    return '(tarteaucitron.job = tarteaucitron.job || []).push(\'facebook\');';
  }

  /**
   * Returns the vanisher name.
   *
   * @return string
   *   The vanisher name.
   */
  public function getVanisherName() {
    return 'facebook';
  }

  /**
   * Returns the name of this vanisher.
   *
   * @return string
   *   The name of this vanisher.
   */
  public function __toString() {
    return 'Facebook Likes';
  }

  /**
   *
   */
  public function getCookies() {
    return [];
  }

  /**
   *
   */
  public function getJavascript() {
    return <<< EOT
function () {
        "use strict";
        tarteaucitron.fallback(['fb-post', 'fb-follow', 'fb-activity', 'fb-send', 'fb-share-button', 'fb-like'], '');
        tarteaucitron.addScript('//connect.facebook.net/' + tarteaucitron.getLocale() + '/sdk.js#xfbml=1&version=v2.0', 'facebook-jssdk');
        if (tarteaucitron.isAjax === true) {
            if (typeof FB !== "undefined") {
                FB.XFBML.parse();
            }
        }
    }
EOT;
  }

  /**
   *
   */
  public function getFallbackJavascript() {
    return <<<EOT
function () {
        "use strict";
        var id = 'facebook';
        tarteaucitron.fallback(['fb-post', 'fb-follow', 'fb-activity', 'fb-send', 'fb-share-button', 'fb-like'], tarteaucitron.engage(id));
    }
EOT;

  }

}
