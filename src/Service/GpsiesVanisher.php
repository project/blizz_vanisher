<?php

namespace Drupal\blizz_vanisher\Service;

/**
 * Class GpsiesVanisher.
 *
 * @package Drupal\blizz_vanisher\Service
 */
class GpsiesVanisher extends IframeVanisher implements IframeVanisherInterface {

  /**
   * {@inheritdoc}
   */
  protected function getIframeSearchRegexPattern() {
    return '~(<iframe.*?src=([\'"])(.*?www\.gpsies\.com.*?)\2.*><\/iframe>)~i';
  }

  /**
   * {@inheritdoc}
   */
  public function getIframeName() {
    return 'gpsies';
  }

  /**
   * {@inheritdoc}
   */
  protected function getIframeData($iframe) {
    $data = parent::getIframeData($iframe);
    $data['name'] = $this->getIframeName();

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getIframePrivacyUrl() {
    return 'http://www.gpsies.com/page.do?page=privacy';
  }

  /**
   * {@inheritdoc}
   */
  public function getIframeCookies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getVanisherName() {
    return 'gpsies';
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return 'Gpsies Vanisher';
  }

  /**
   *
   */
  public function getCookies() {
    return [];
  }

  /**
   *
   */
  public function getJavascript() {
    return <<< EOT
function () {
    "use strict";
    tarteaucitron.fallback(['tac_iframe'], function (x) {
      var width = x.getAttribute("width"),
          height = x.getAttribute("height"),
          url = x.getAttribute("data-url");

      return '<iframe src="' + url + '" width="' + width + '" height="' + height + '" frameborder="0" scrolling="no" allowtransparency allowfullscreen></iframe>';
    });
  }
EOT;
  }

  /**
   *
   */
  public function getFallbackJavascript() {
    return <<<EOT
function () {
    "use strict";
    var id = 'iframe';
    tarteaucitron.fallback(['tac_iframe'], function (elem) {
      elem.style.width = elem.getAttribute('width') + 'px';
      elem.style.height = elem.getAttribute('height') + 'px';
      return tarteaucitron.engage(id);
    });
  }
EOT;

  }

  /**
   * Returns the replacement script.
   *
   * @return string
   *   The replacement script.
   */
  protected function getReplacementScript() {
    return '(tarteaucitron.job = tarteaucitron.job || []).push(\'gpsies\');';
  }

}
