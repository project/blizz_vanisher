<?php

namespace Drupal\blizz_vanisher\Service;

/**
 * Class GoogleTagManagerVanisher.
 *
 * @package Drupal\blizz_vanisher\Service
 */
class GoogleTagManagerVanisher extends ThirdPartyServicesVanisher implements ThirdPartyServicesVanisherInterface {

  /**
   * {@inheritdoc}
   */
  public function vanish(&$content) {
    $replaced_scripts = [];

    $scripts = $this->getScripts('googletagmanager.com/gtm.js', $this->getAllScripts($content));
    foreach ($scripts as $script) {
      $gtm_id = $this->getGtmId($script);

      if ($gtm_id) {
        $replaced_scripts[] = $this->getReplacementScript($gtm_id);

        // Remove the original script.
        $content = $this->removeScript($script, $content);
      }
    }

    $replaced_scripts[] = '(tarteaucitron.job = tarteaucitron.job || []).push(\'googletagmanager\');';

    return implode("\n", $replaced_scripts);
  }

  /**
   * Returns the replacement script.
   *
   * @param string $gtm_id
   *   The google tag manager id.
   *
   * @return string
   *   The replacement script.
   */
  public function getReplacementScript($gtm_id) {
    return 'tarteaucitron.user.googletagmanagerId = \'' . $gtm_id . '\';';
  }

  /**
   * Returns the google tag manager id.
   *
   * @param string $script
   *   The script containing the google tag manager id.
   *
   * @return string
   *   The google tag manager id.
   *
   * @throws \Exception
   *   When no google tag manager id has been found.
   */
  protected function getGtmId($script) {
    $matches = [];
    if (FALSE === preg_match("~'(GTM\-.*?)'~s", $script, $matches)) {
      throw new \Exception('Could not find google tag manager id in script.');
    }

    return !empty($matches[1]) ? $matches[1] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getVanisherName() {
    return 'googletagmanager';
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return 'Google Tag Manager Vanisher';
  }

  /**
   * {@inheritdoc}
   */
  public function getCookies() {
    return [
      '_ga',
      '_gat',
      '__utma',
      '__utmb',
      '__utmc',
      '__utmt',
      '__utmz',
      '__gads',
      '_drt_',
      'FLC',
      'exchange_uid',
      'id',
      'fc',
      'rrs',
      'rds',
      'rv',
      'uid',
      'UIDR',
      'UID',
      'clid',
      'ipinfo',
      'acs',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getJavascript() {
    return <<< EOT
function () {
        if (tarteaucitron.user.googletagmanagerId === undefined) {
            return;
        }
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        tarteaucitron.addScript('//www.googletagmanager.com/gtm.js?id=' + tarteaucitron.user.googletagmanagerId);
    }
EOT;
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackJavascript() {
    return '';
  }

}
