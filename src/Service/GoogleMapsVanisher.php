<?php

namespace Drupal\blizz_vanisher\Service;

/**
 * Class GoogleMapsVanisher.
 *
 * @package Drupal\blizz_vanisher\Service
 */
class GoogleMapsVanisher extends IframeVanisher implements ThirdPartyServicesVanisherInterface, IframeVanisherInterface {

  /**
   * Returns the regular expression pattern to search for the iframe.
   *
   * @return string
   *   The regular expression pattern.
   */
  protected function getIframeSearchRegexPattern() {
    return '~(<iframe[^>]*?src=([\'"])[^\'"]*?google\.com/maps.*?\2.*?>.*?</iframe>)~is';
  }

  /**
   * Returns the vanisher name.
   *
   * @return string
   *   The vanisher name.
   */
  public function getVanisherName() {
    return 'googlemaps';
  }

  /**
   * Returns the name of this vanisher.
   *
   * @return string
   *   The name of this vanisher.
   */
  public function __toString() {
    return 'Google Maps';
  }

  /**
   *
   */
  public function getCookies() {
    return [];
  }

  /**
   *
   */
  public function getJavascript() {
    return <<<EOT
 function () {
        "use strict";
        var mapOptions,
            map,
            uniqIds = [],
            i;

        if (tarteaucitron.user.mapscallback === undefined) {
            tarteaucitron.user.mapscallback = 'tac_googlemaps_callback';
        }

        tarteaucitron.addScript('//maps.googleapis.com/maps/api/js?v=3.exp&key=' + tarteaucitron.user.googlemapsKey + '&callback='+tarteaucitron.user.mapscallback);

        window.tac_googlemaps_callback = function () {
            tarteaucitron.fallback(['googlemaps-canvas'], function (x) {
                var uniqId = '_' + Math.random().toString(36).substr(2, 9);
                uniqIds.push(uniqId);
                return '<div id="' + uniqId + '" zoom="' + x.getAttribute('zoom') + '" latitude="' + x.getAttribute('latitude') + '" longitude="' + x.getAttribute('longitude') + '" style="width:' + x.offsetWidth + 'px;height:' + x.offsetHeight + 'px"></div>';
            });
        
            for (i = 0; i < uniqIds.length; i += 1) {
                mapOptions = {
                    zoom: parseInt(document.getElementById(uniqIds[i]).getAttribute('zoom'), 10),
                    center: new google.maps.LatLng(parseFloat(document.getElementById(uniqIds[i]).getAttribute('latitude'), 10), parseFloat(document.getElementById(uniqIds[i]).getAttribute('longitude'), 10))
                };
                map = new google.maps.Map(document.getElementById(uniqIds[i]), mapOptions);
            }
        };
    }
EOT;

  }

  /**
   *
   */
  public function getFallbackJavascript() {
    return <<< EOT
function () {
        "use strict";
        var id = 'googlemaps';
        tarteaucitron.fallback(['googlemaps-canvas'], tarteaucitron.engage(id));
    }
EOT;

  }

}
