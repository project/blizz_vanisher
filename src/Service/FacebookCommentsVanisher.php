<?php

namespace Drupal\blizz_vanisher\Service;

/**
 * Class FacebookCommentsVanisher.
 *
 * @package Drupal\blizz_vanisher\Service
 */
class FacebookCommentsVanisher extends ThirdPartyServicesVanisher implements ThirdPartyServicesVanisherInterface {

  /**
   * {@inheritdoc}
   */
  public function vanish(&$content) {
    $script = $this->getScript('connect.facebook.net', $this->getAllScripts($content));

    // Remove the script from the content.
    $content = $this->removeScript($script, $content);

    return $this->getReplacementScript();
  }

  /**
   * Returns the replacement script.
   *
   * @return string
   *   The replacement script.
   */
  public function getReplacementScript() {
    return '(tarteaucitron.job = tarteaucitron.job || []).push(\'facebookcomment\');';
  }

  /**
   * Returns the vanisher name.
   *
   * @return string
   *   The vanisher name.
   */
  public function getVanisherName() {
    return 'facebookcomment';
  }

  /**
   * Returns the name of this vanisher.
   *
   * @return string
   *   The name of this vanisher.
   */
  public function __toString() {
    return 'Facebook Comments';
  }

  /**
   *
   */
  public function getCookies() {
    return [];
  }

  /**
   *
   */
  public function getJavascript() {
    return <<< EOT
function () {
        "use strict";
        tarteaucitron.fallback(['fb-comments'], '');
        tarteaucitron.addScript('//connect.facebook.net/' + tarteaucitron.getLocale() + '/sdk.js#xfbml=1&version=v2.0', 'facebook-jssdk');
        if (tarteaucitron.isAjax === true) {
            if (typeof FB !== "undefined") {
                FB.XFBML.parse();
            }
        }
    }
EOT;
  }

  /**
   *
   */
  public function getFallbackJavascript() {
    return <<< EOT
 function () {
        "use strict";
        var id = 'facebookcomment';
        tarteaucitron.fallback(['fb-comments'], tarteaucitron.engage(id));
    }
EOT;

  }

}
