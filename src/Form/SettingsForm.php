<?php

namespace Drupal\blizz_vanisher\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\blizz_vanisher\Form
 */
class SettingsForm extends ConfigFormBase {

  const SETTINGS_NAME = 'blizz_vanisher.settings';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['blizz_vanisher_hashtag'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hashtag'),
      '#description' => $this->t('Automatically open the panel with the hashtag.'),
      '#default_value' => $this->config(self::SETTINGS_NAME)->get('blizz_vanisher_hashtag') ?: '',
    ];

    $form['blizz_vanisher_high_privacy'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('High Privacy'),
      '#description' => $this->t('Disabling the auto consent feature on navigation?'),
      '#default_value' => $this->config(self::SETTINGS_NAME)->get('blizz_vanisher_high_privacy') ? TRUE : FALSE,
    ];

    $form['blizz_vanisher_high_privacy_accept_all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('High Privacy accept all'),
      '#description' => $this->t('Show the Accept all button when in High Privacy mode'),
      '#default_value' => $this->config(self::SETTINGS_NAME)->get('blizz_vanisher_high_privacy_accept_all') ? TRUE : FALSE,
    ];

    $form['blizz_vanisher_orientation'] = [
      '#type' => 'radios',
      '#title' => $this->t('Orientation'),
      '#description' => $this->t('The orientation of the big banner.'),
      '#default_value' => $this->config(self::SETTINGS_NAME)->get('blizz_vanisher_orientation') ?: 'top',
      '#options' => [
        'top' => $this->t('Top'),
        'bottom' => $this->t('Bottom'),
      ],
    ];

    $form['blizz_vanisher_adblocker'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ad-Blocker'),
      '#description' => $this->t('Display a message if an adblocker is detected.'),
      '#default_value' => $this->config(self::SETTINGS_NAME)->get('blizz_vanisher_adblocker') ? TRUE : FALSE,
    ];

    $form['blizz_vanisher_show_alert_small'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Alert Small'),
      '#description' => $this->t('Show the small banner on bottom right?'),
      '#default_value' => $this->config(self::SETTINGS_NAME)->get('blizz_vanisher_show_alert_small') ? TRUE : FALSE,
    ];

    $form['blizz_vanisher_cookieslist'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Cookies List'),
      '#description' => $this->t('Display the list of cookies installed?'),
      '#default_value' => $this->config(self::SETTINGS_NAME)->get('blizz_vanisher_cookieslist') ? TRUE : FALSE,
    ];

    $form['blizz_vanisher_remove_credit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove Credit'),
      '#description' => $this->t('Remove the credit link?'),
      '#default_value' => $this->config(self::SETTINGS_NAME)->get('blizz_vanisher_remove_credit') ? TRUE : FALSE,
    ];

    $form['blizz_vanisher_default_rejected'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Default Rejected'),
      '#description' => $this->t('Should all services be rejected by default?'),
      '#default_value' => $this->config(self::SETTINGS_NAME)->get('blizz_vanisher_default_rejected') ? TRUE : FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory()->getEditable(self::SETTINGS_NAME)
      ->set('blizz_vanisher_hashtag', $form_state->getValue('blizz_vanisher_hashtag', ''))
      ->set('blizz_vanisher_high_privacy', $form_state->getValue('blizz_vanisher_high_privacy', FALSE))
      ->set('blizz_vanisher_high_privacy_accept_all', $form_state->getValue('blizz_vanisher_high_privacy_accept_all', FALSE))
      ->set('blizz_vanisher_orientation', $form_state->getValue('blizz_vanisher_orientation', 'top'))
      ->set('blizz_vanisher_adblocker', $form_state->getValue('blizz_vanisher_adblocker', FALSE))
      ->set('blizz_vanisher_show_alert_small', $form_state->getValue('blizz_vanisher_show_alert_small', FALSE))
      ->set('blizz_vanisher_cookieslist', $form_state->getValue('blizz_vanisher_cookieslist', FALSE))
      ->set('blizz_vanisher_remove_credit', $form_state->getValue('blizz_vanisher_remove_credit', FALSE))
      ->set('blizz_vanisher_default_rejected', $form_state->getValue('blizz_vanisher_default_rejected', FALSE))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return [
      self::SETTINGS_NAME,
    ];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'blizz_vanisher.settings_form';
  }

}
