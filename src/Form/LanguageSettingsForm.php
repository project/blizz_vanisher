<?php

namespace Drupal\blizz_vanisher\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LanguageSettingsForm.
 *
 * @package Drupal\blizz_vanisher\Form
 */
class LanguageSettingsForm extends ConfigFormBase {

  const SETTINGS_NAME = 'blizz_vanisher.language.settings';

  const FIELDS = [
    'adblock' => '',
    'adblock_call' => '',
    'reload' => '',
    'alertBigScroll' => '',
    'alertBigClick' => '',
    'alertBig' => '',
    'alertBigPrivacy' => '',
    'alertSmall' => '',
    'personalize' => '',
    'acceptAll' => '',
    'close' => '',
    'all' => '',
    'info' => '',
    'disclaimer' => '',
    'allow' => '',
    'allowAll'=> '',
    'deny' => '',
    'denyAll' => '',
    'noCookie' => '',
    'useCookie' => '',
    'useCookieCurrent' => '',
    'useNoCookie' => '',
    'more' => '',
    'source' => '',
    'credit' => '',
    'fallback' => '',
    'ads' => ['title' => '', 'details' => ''],
    'analytic' => ['title' => '', 'details' => ''],
    'social' => ['title' => '', 'details' => ''],
    'video' => ['title' => '', 'details' => ''],
    'comment' => ['title' => '', 'details' => ''],
    'support' => ['title' => '', 'details' => ''],
    'api' => ['title' => '', 'details' => ''],
    'other' => ['title' => '', 'details' => ''],
  ];

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;
    $this->createTextField(self::FIELDS, $form);

    return parent::buildForm($form, $form_state);
  }

  /**
   *
   */
  private function createTextField($fields, &$form, $old_key = NULL) {
    foreach ($fields as $key => $value) {
      if (is_string($value)) {

        if ($old_key != NULL) {
          $default_value = $this->config(self::SETTINGS_NAME)
            ->get($old_key)[$key] ?: '';
        }
        else {
          $default_value = $this->config(self::SETTINGS_NAME)
            ->get($key) ?: '';
        }

        $form[$key] = [
          '#type' => 'textarea',
          '#title' => $key,
          '#description' => $value,
          '#default_value' => $default_value,
        ];
      }
      else {
        $form[$key] = [
          '#type' => 'details',
          '#open' => TRUE,
          '#title' => $this->t($key),
        ];
        $this->createTextField($value, $form[$key], $key);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->configFactory()->getEditable(self::SETTINGS_NAME);

    foreach (self::FIELDS as $key => $value) {
      $settings->set($key, $form_state->getValue($key, ''));
    }

    $settings->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return [
      self::SETTINGS_NAME,
    ];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'blizz_vanisher.language.settings_form';
  }

}
