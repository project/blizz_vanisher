<?php

namespace Drupal\blizz_vanisher\Form;

use Drupal\blizz_vanisher\Service\ThirdPartyServicesVanisher;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ThirdPartyServiceForm.
 *
 * @package Drupal\blizz_vanisher\Form
 */
class ThirdPartyServiceForm extends EntityForm {

  /**
   * The third party services vanisher.
   *
   * @var \Drupal\blizz_vanisher\Service\ThirdPartyServicesVanisher
   */
  protected $vanisher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('blizz_vanisher.service.third_party_services_vanisher')
    );
  }

  /**
   * ThirdPartyServiceForm constructor.
   *
   * @param \Drupal\blizz_vanisher\Service\ThirdPartyServicesVanisher $vanisher
   *   The third party services vanisher.
   */
  public function __construct(ThirdPartyServicesVanisher $vanisher) {
    $this->vanisher = $vanisher;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $types = [
      'api' => $this->t('API Like Services'),
      'analytic' => $this->t('Tracking'),
      'ads' => $this->t('Advertising'),
      'comment' => $this->t('Comments'),
      'other' => $this->t('Others'),
      'social' => $this->t('Social'),
      'support' => $this->t('Support'),
      'video' => $this->t('Video'),
    ];

    /** @var \Drupal\blizz_vanisher\Entity\ThirdPartyServiceEntityInterface $third_party_service */
    $third_party_service = $this->entity;

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('This is the name of the service.'),
      '#default_value' => $third_party_service->getName() ?: '',
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $third_party_service->getId(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
        'source' => ['name'],
      ],
      '#disabled' => !$third_party_service->isNew(),
    ];

    $form['vanisher'] = [
      '#type' => 'select',
      '#title' => 'Vanisher',
      '#description' => $this->t('Choose the vanisher for the service control.'),
      '#default_value' => $third_party_service->getVanisher() ?: '',
      '#options' => $this->vanisher->getInstalled() ?: [],
      '#empty_value' => '',
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t(''),
      '#default_value' => $third_party_service->getDescription() ?: '',
      '#required' => FALSE,
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose a Type'),
      '#description' => $this->t(''),
      '#options' => $types,
      '#default_value' => $third_party_service->getGroupType() ?: '',
      '#required' => TRUE,
    ];
    $form['needConsent'] = [
      '#type' => 'select',
      '#title' => $this->t('Need Consent'),
      '#description' => $this->t(''),
      '#options' => [TRUE => 'True', FALSE => 'False'],
      '#default_value' => $third_party_service->needConsent() ?: TRUE,
      '#required' => TRUE,
    ];

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Service Control?'),
      '#description' => $this->t('If activated this service will be controlled with tarteaucitron.'),
      '#default_value' => $third_party_service->isEnabled() ?: FALSE,
    ];

    $form['optional_fields'] = [
      '#type' => 'details',
      '#title' => $this->t('Optional Fields'),
    ];

    $form['optional_fields']['info'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Info Content'),
      '#description' => $this->t('This content will be displayed instead until the user activates the service.'),
      '#default_value' => $third_party_service->getInfo() ?: '',
      '#required' => FALSE,
    ];

    $form['optional_fields']['readMore'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Read More '),
      '#description' => $this->t(''),
      '#default_value' => $third_party_service->getReadMore() ?: '',
      '#required' => FALSE,
    ];

    $form['optional_fields']['uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Officially Website Url '),
      '#description' => $this->t(''),
      '#default_value' => $third_party_service->getLinkToOfficialWebsite() ?: '',
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\blizz_vanisher\Entity\ThirdPartyServiceEntityInterface $third_party_service */
    $third_party_service = $this->entity;
    $status = $third_party_service->save();

    if ($status) {
      drupal_set_message($this->t('Saved the %label Third Party Service.', [
        '%label' => $third_party_service->getLabel(),
      ]));
    }
    else {
      drupal_set_message($this->t('The %label Third Party Service was not saved.', [
        '%label' => $third_party_service->getLabel(),
      ]));
    }

    $form_state->setRedirect('entity.third_party_service.collection');
  }

  /**
   * Checks whether a Third Party Service configuration entity exists.
   *
   * @param string $id
   *   The id of the entity.
   *
   * @return bool
   *   TRUE if existing, otherwise FALSE.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   When the plugin definition is invalid.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('third_party_service')
      ->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
