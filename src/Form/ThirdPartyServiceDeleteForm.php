<?php

namespace Drupal\blizz_vanisher\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ThirdPartyServiceDeleteForm.
 *
 * @package Drupal\blizz_vanisher\Form
 */
class ThirdPartyServiceDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', [
      '%name' => $this->entity->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return 'entity.third_party_service.collection';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    drupal_set_message($this->t('Third Party Service %label has been deleted.', [
      '%label' => $this->entity->label(),
    ]));

    $form_state->setRedirect($this->getCancelUrl());
  }

}
