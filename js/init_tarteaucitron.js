var tarteaucitronCustomText = drupalSettings.blizz_vanisher.langConfig;

tarteaupomme.init({
  "hashtag": drupalSettings.blizz_vanisher.hashtag, /* Automatically open the panel with the hashtag */
  "highPrivacy": Boolean(drupalSettings.blizz_vanisher.highPrivacy), /* disabling the auto consent feature on navigation? */
  "orientation": drupalSettings.blizz_vanisher.orientation, /* the big banner should be on 'top' or 'bottom'? */
  "adblocker": Boolean(drupalSettings.blizz_vanisher.adblocker), /* Display a message if an adblocker is detected */
  "showAlertSmall": Boolean(drupalSettings.blizz_vanisher.showAlertSmall), /* show the small banner on bottom right? */
  // "showAlertSmall": true, /* show the small banner on bottom right? */
  "cookieslist": Boolean(drupalSettings.blizz_vanisher.cookieslist), /* Display the list of cookies installed ? */
  "removeCredit": Boolean(drupalSettings.blizz_vanisher.removeCredit), /* remove the credit link? */
  "defaultRejected": Boolean(drupalSettings.blizz_vanisher.defaultRejected), /* Should the services be rejected by default? */
  "AcceptAllCta" : Boolean(drupalSettings.blizz_vanisher.AcceptAllCta), /* Show the accept all button when highPrivacy on */
});
